import React, {Component} from 'react';
import './App.css';
import Counter from "./Counter/Counter";
import Layout from "./Layout/Layout";
import {BrowserRouter, Switch, Route} from "react-router-dom";
import ToDo from "./ToDo/ToDo";
import ToDoEdit from "./ToDoEdit/ToDoEdit";

class App extends Component {
    render() {
        return (
            <div className="App">
                <BrowserRouter>
                    <Layout>
                        <Switch>
                            <Route path="/" exact component={Counter}/>
                            <Route path="/todo" component={ToDo}/>
                            <Route path="/tasks/:id/edit" component={ToDoEdit}/>
                        </Switch>
                    </Layout>
                </BrowserRouter>
            </div>
        );
    }
}

export default App;
