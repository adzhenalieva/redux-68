import React, {Component} from 'react';

import './ToDoForm.css'

class ToDoForm extends Component {
    constructor(props) {
        super(props);
        if (props.tasks) {
            this.state = {...props.tasks};
        } else {
            this.state = {
                task: null
            };
        }
    }

    valueChanged = event => {
        const {name, value} = event.target;

        this.setState({[name]: value})
    };

    submitHandler = event => {
        event.preventDefault();
        this.props.onSubmit({...this.state});
    };

    render() {
        return (
            <form className="ToDoForm" onSubmit={this.submitHandler}>
                <p>ToDo text: </p>
                <input name="task" value={this.state.task}
                          onChange={this.valueChanged} className="InputToDo"/>
                <button type="submit" className="Save">Save</button>
            </form>
        );
    }
}

export default ToDoForm;