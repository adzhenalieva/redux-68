import axios from '../axios-counter';

export const INCREMENT = 'INCREMENT';
export const DECREMENT = 'DECREMENT';
export const ADD = 'ADD';
export const SUBTRACT = 'SUBTRACT';

export const FETCH_COUNTER_REQUEST = 'FETCH_COUNTER_REQUEST';
export const FETCH_COUNTER_SUCCESS = 'FETCH_COUNTER_SUCCESS';
export const FETCH_COUNTER_ERROR = 'FETCH_COUNTER_ERROR';

export const FETCH_TODO_REQUEST = 'FETCH_TODO_REQUEST';
export const FETCH_TODO_SUCCESS = 'FETCH_TODO_SUCCESS';
export const FETCH_TODO_ERROR = 'FETCH_TODO_ERROR';

export const FOCUS = 'FOCUS';
export const CHANGE_VALUE = 'CHANGE_VALUE';
export const ADD_TASK = 'ADD_TASK';
export const CHANGE_TASK = 'CHANGE_TASK';

export const fetchCounterRequest = () => {
    return {type: FETCH_COUNTER_REQUEST};
};

export const fetchCounterSuccess = (counter) => {
    return {type: FETCH_COUNTER_SUCCESS, counter};
};

export const fetchCounterError = () => {
    return {type: FETCH_COUNTER_ERROR};
};

export const fetchToDoRequest = () => {
    return {type: FETCH_TODO_REQUEST};
};

export const fetchToDoSuccess = (tasks) => {
    return {type: FETCH_TODO_SUCCESS, tasks};
};

export const fetchToDoError = () => {
    return {type: FETCH_TODO_ERROR};
};

export const fetchCounter = () => {
    return dispatch => {
        dispatch(fetchCounterRequest());
        axios.get('/counter.json').then(response => {
            dispatch(fetchCounterSuccess(response.data));
        }, error => {
            dispatch(fetchCounterError());
        });
    }
};

export const fetchToDo = () => {
    return dispatch => {
        dispatch(fetchToDoRequest());
        axios.get('/tasks.json').then(response => {
            let tasks = [];
            for (let key in response.data) {
                let task = {};
                task.text = response.data[key].task;
                task.id = key;
                tasks.push(task);
            }
            dispatch(fetchToDoSuccess(tasks));
        }, error => {
            dispatch(fetchToDoError());
        });
    }
};

export const incrementCounter = () => {

    return dispatch => {
        dispatch(sendData());
        dispatch({type: INCREMENT});
    };
};

export const decrementCounter = () => {
    return dispatch => {
        dispatch(sendData());
        dispatch({type: DECREMENT});
    };

};

export const addCounter = (amount) => {
    return dispatch => {
        dispatch(sendData());
        dispatch({type: ADD, amount});
    };
};

export const subtractCounter = (amount) => {
    return dispatch => {
        dispatch(sendData());
        dispatch({type: SUBTRACT, amount});
    };
};

export const sendData = () => {
    return (dispatch, getState) => {
        const state = getState();
        const counter = state.counter;
        axios.put('/counter.json', counter)
    }
};

export const onFocusToDo = () => {
    return {type: FOCUS};

};

export const changeValueToDo = (value) => {
    return {type: CHANGE_VALUE, value};

};

export const addTask = (event) => {
    event.preventDefault();
    return (dispatch, getState) => {
        const state = getState();
        const task = {task: state.value};
        axios.post('tasks.json', task).then(() => {
            dispatch({type: ADD_TASK});
            dispatch(fetchToDo());
        });
    }
};

export const removeTask = (id) => {
    return (dispatch) => {
        axios.delete('/tasks/' + id + '.json').then(() => {
            dispatch(fetchToDo());
        });
    }
};