import {
    ADD, ADD_TASK, CHANGE_TASK,
    CHANGE_VALUE,
    DECREMENT,
    FETCH_COUNTER_REQUEST,
    FETCH_COUNTER_SUCCESS, FETCH_TODO_REQUEST, FETCH_TODO_SUCCESS,
    FOCUS,
    INCREMENT,
    SUBTRACT
} from "./actions";

const initialState = {
    counter: 0,
    loading: false,
    newLoading: false,
    value: 'Add new task',
    tasks: []

};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case INCREMENT:
            return {
                ...state,
                counter: state.counter + 1
            };
        case ADD:
            return {
                ...state,
                counter: state.counter + action.amount
            };
        case DECREMENT:
            return {
                ...state,
                counter: state.counter - 1
            };
        case SUBTRACT:
            return {
                ...state,
                counter: state.counter - action.amount
            };
        case FETCH_COUNTER_SUCCESS:
            return {
                ...state, counter: action.counter,
                loading: false
            };
        case FETCH_COUNTER_REQUEST:
            return {
                ...state, loading: true
            };
        case FOCUS:
            return {
                ...state, value: ''
            };
        case CHANGE_VALUE:
            return {
                ...state, value: action.value
            };
        case CHANGE_TASK:
            return {
                ...state
            };
        case ADD_TASK:
            return {
                ...state, value: ''
            };
        case FETCH_TODO_SUCCESS:
            return {
                ...state, tasks: action.tasks,
                newLoading: false
            };
        case FETCH_TODO_REQUEST:
            return {
                ...state, newLoading: true
            };
        default:
            return state
    }
};

export default reducer;