import React, {Component, Fragment} from 'react';
import './ToDo.css';
import {connect} from 'react-redux';
import {addTask, changeValueToDo, fetchToDo, onFocusToDo, removeTask} from "../store/actions";
import Spinner from "../UI/Spinner/Spinner";


class ToDo extends Component {

    componentDidMount() {
        this.props.fetchToDo();
    }

    editToDo = (id) => {
        this.props.history.push({
            pathname: '/tasks/' + id + '/edit'
        });
    };

    render() {
        return (
            <div>
                {this.props.loading ? <Spinner/> :
                    <Fragment>
                        <form>
                            <p>
                                <input className="Input" type="text" value={this.props.value}
                                       onFocus={this.props.onFocus}
                                       onChange={this.props.changeValue}/>
                            </p>
                            <button className="btnAdd" onClick={this.props.onClick}>Add</button>
                        </form>

                        {this.props.tasks.map((task, index) =>
                            <div className="AddedDiv" key={task.id}>
                                <input
                                    className="AddedInput" type="text" value={task.text}
                                    disabled />
                                <button className="btnEdit" onClick={() => this.editToDo(task.id)}>edit</button>
                                <button className="btnDelete" onClick={() => this.props.removeTask(task.id)}>delete</button>
                            </div>
                        )}
                    </Fragment>
                }
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        value: state.value,
        tasks: state.tasks,
        loading: state.newLoading
    }
};

const mapDispatchToProps = dispatch => {
    return {
        onFocus: () => dispatch(onFocusToDo()),
        changeValue: (event) => dispatch(changeValueToDo(event.target.value)),
        onClick: (event) => dispatch(addTask(event)),
        fetchToDo: () => dispatch(fetchToDo()),
        removeTask: (id) => dispatch(removeTask(id))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(ToDo);