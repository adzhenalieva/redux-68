import axios from 'axios';

const instance = axios.create({
    baseURL: 'https://services-hw66.firebaseio.com/'
});


export default instance;


