import React, {Component} from 'react';
import Spinner from "../UI/Spinner/Spinner";
import './ToDoEdit.css'
import ToDoForm from "../ToDoForm/ToDoForm";

import axios from '../axios-counter';

class ToDoEdit extends Component {
    state = {
        tasks: null
    };

    getUrl = () => {
        const id = this.props.match.params.id;
        return 'tasks/' + id + '.json'
    };

    componentDidMount() {
        axios.get(this.getUrl()).then(response => {
            this.setState({tasks: response.data})
        })
    }

    editTask = tasks => {
        axios.put(this.getUrl(), tasks).then(() => {
            this.props.history.replace('/todo');
        })
    };

    render() {
        let formTask = <ToDoForm
            onSubmit={this.editTask}
            tasks={this.state.tasks}
        />;

        if (!this.state.tasks) {
            formTask = <Spinner />
        }
        return (
            <div>
                <h2>Edit task: </h2>
                {formTask}
            </div>
        );
    }
}

export default ToDoEdit;