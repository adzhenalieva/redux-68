import React from 'react';
import {NavLink} from "react-router-dom";
import './Toolbar.css';

const Toolbar = () => {
    return (
            <nav className="MainNav">
                <NavLink className="NavLink" activeClassName="Active" to="/" exact={true} >Home</NavLink>
                <NavLink className="NavLink" activeClassName="Active"  to="/todo" >ToDo</NavLink>
            </nav>
    );
};

export default Toolbar;